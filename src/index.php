<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 11.11.2017
 * Time: 19:13
 */
?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Hello World</title>
</head>
<script src="../js/pixi.min.js"></script>
<body>
<script type="text/javascript">
    var type = "WebGL";
    if (!PIXI.utils.isWebGLSupported()) {
        type = "canvas"
    }

    PIXI.utils.sayHello(type)
</script>
<style type="text/css">

    * {
        padding: 0;
        margin: 0;
    }

</style>
<script type="text/javascript" src="js/func.js">


</script>
</body>
</html>