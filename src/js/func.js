/**
 * Created by Andi on 11/27/2017.
 */

// var readline = require('linebyline');

var Container = PIXI.Container,
    autoDetectRenderer = PIXI.autoDetectRenderer,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    Sprite = PIXI.Sprite,
    Rectangle = PIXI.Rectangle,
    TextureCache = PIXI.TextureCache;
var man = "";
var stone = [];
var state;

//Create a pixi stage and rednerer and add the
//renderer.view to the dom
var stage = new Container(),
    renderer = autoDetectRenderer(256, 256);
document.body.appendChild(renderer.view);
loader.add("../images/man.png")
    .add("../images/stein1klein.png").load(setup);

var message;
//load an image and urn the 'setup' function when its done
//    loader.add("manImage", "../images/spritesheet.jpg")
//        .on("progress", loadProgressHandler)
//        .load(setup);


//    function loadProgressHandler(loader, resource) {
//        //Display the file 'url' currently being loaded
//        console.log("loading" + resource.url);
//
//        // Display the percentage of files currently loaded
//        console.log("progress: " + loader.progress + "%");
//
//        //If you gave your files names at the first argument
//        // of the 'add' method, you cann access them like this
//        console.log("loading: " + resource.name);
//
//    }

renderer.backgroundColor = 0x061638;
renderer.autoResize = true;
renderer.resize(window.innerWidth, window.innerHeight);
//    scaleToWindow(renderer.view, 0x061639);
//Add canvas to HTML document
document.body.appendChild(renderer.view);

//Create container object called the 'stage'
//    var stage = new Container();
var baseImage = "";
var rectangle = new PIXI.Graphics();

function initThreeSprites() {
    var move = [];
    var x = 0;
    var y = (140 * 3) - 3;
    var width = 100;
    var heigth = 140;
    for (var i = 1; i <= 3; i++) {
        move[i] = new Rectangle(x + 100, y, width, heigth);
        x += 100;
    }

    return move;
}
function initSprites() {
    var move = [];
    var x = 300;
    var y = (140 * 3) - 3;
    var width = 130;
    var heigth = 140;
    for (var i = 1; i <= 3; i++) {
        move[i] = new Rectangle(x + 100, y, width, heigth);
        x += 100;
    }

    return move;
}

var start = 0;


function setup() {

    message = new PIXI.Text(
        "No collission...",
        {fontFamily: "Arial", fontSize: 32, fill: "white"}
    );

//creating rectangle

    // rectangle = new PIXI.Graphics();

    rectangle.beginFill(0x66CCFF);
    rectangle.drawRect(0, 0, 100, 100);
    rectangle.x = 500;
    rectangle.y = 140;
    rectangle.endFill();


    //Create the 'tileset' sprite from the texture
    baseImage = loader.resources["../images/man.png"].texture;
    stoneImage = loader.resources["../images/stein1klein.png"].texture;

    //create a rectangle object that defines the position and
    //size of the sub-image you want to extract from the texture
    var standingMove = new Rectangle(170, 140, 78, 140);
    // var standingMove = new Rectangle(0, (140 * 3) - 3, 100, 140);


    //Tell the texture to use that rectangular section
    baseImage.frame = standingMove;


    //create the sprite from the texture
    man = new Sprite(baseImage);
    man.y = 96;
    man.vx = 0;
    man.vy = 0;

    for(var i = 0; i < 50; i++) {

        stone[i] = new Sprite(stoneImage);
        stone[i].y = 96+i*50;
        stone[i].width = 80;
        stone[i].height = 60;
        stone[i].x = 800;
        stone[i].vx = 0;
        stone[i].vy = 0;
    }



    //create the sprite from the texture
    man = new Sprite(baseImage);
    man.y = 96;
    man.vx = 0;
    man.vy = 0;

//
//        //position the rocket sprite on the canvas
//        man.x = 64;
//        man.y = 64;
//
//        //initalize the man's velocity variables
//        man.vx = 0;
//        man.vy = 0;

    stage.addChild(rectangle);
    stage.addChild(message);
    stage.addChild(man);
    stage.addChild(stone[0]);
    stage.addChild(stone[1]);
    stage.addChild(stone[2]);
    stage.addChild(stone[3]);
    stage.addChild(stone[4]);
    stage.addChild(stone[5]);
    stage.addChild(stone[6]);
    stage.addChild(stone[7]);

//        var keyObject = keyboard(asciiKeyCodeNumber);
//
//        keyObject.press = function () {
//            //key object pressed
//        };
//
//        keyObject.release = function () {
//            //key object released
//        };

    //capture the keyboard arrow keys
    var left = keyboard(37),
        up = keyboard(38),
        right = keyboard(39),
        down = keyboard(40);

    //left arrow key 'press' method
    left.press = function () {

//            change the cat's velocity when the key is pressed
        man.vx = -5;
        man.vy = 0;

    };

    //Left arrow key 'release' method
    left.release = function () {

        //if the left arrow has been released, and the right arrow isn't down,
        // and the man isn't moving vertically:
        //stop the man
        if (!right.isDown && man.vy === 0) {
            man.vx = 0;
        }
    };

    //Up
    up.press = function () {
        man.vy = -5;
        man.vx = 0;
    };
    up.release = function () {
        if (!down.isDown && man.vx === 0) {
            man.vy = 0;
        }
    };
    //Right
    right.press = function () {


        ++start;
        man.vx = 5;
        man.vy = 0;
        var sprites = initThreeSprites();
        var sprites2 = initSprites();

        if (start <= 3) {

            baseImage.frame = sprites[start];
        }
        if (start > 3 && start <= 7) {
            baseImage.frame = sprites2[start];
        } else {
            start = 0;
        }


    };
    var isPressed = false;
    right.release = function () {
        if (!left.isDown && man.vy === 0) {
            man.vx = 0;
        }
        baseImage.frame = standingMove;
        isPressed = true;
    };

    down.press = function () {
        man.vy = 5;
        man.vx = 0;
    };
    down.release = function () {
        if (!up.isDown && man.vx === 0) {
            man.vy = 0;
        }
    };

    //set the game state
    state = play;


//        var sprite = new PIXI.Sprite(loader.resources['../images/man.png'].texture);
//        sprite.visible = false;

    // set the image in the middle
//        sprite.position.set(96, 96);

    //change the sprite's size
//        sprite.width = 80;
//        sprite.heigth = 120;

    //change the scale
//        sprite.scale.set(0.5, 0.5);

    //change the ancor of the sprite
//        sprite.anchor.set(0.5, 0.5);

    //change the rotation
//        sprite.rotation = 0.5;


    //Tell the renderer to render the stage


    console.log("All files loaded");


    //start the game loop
    gameLoop();


}

function gameLoop() {
    //Loop this function at 60 frames per second
    requestAnimationFrame(gameLoop);
    //update the man's velocity
//        man.vx = 1;
//        man.vy = 1;

    //Move the image 1 pixel to the right each frame
//        man.x += man.vx;
//        man.y += man.vy;

    state();

    renderer.render(stage);

}

function play() {

    // move the man 1 pixel each frame
    man.x += man.vx;
    man.y += man.vy;

    //check for a collision between the cat and the box
    if (hitTestRectangle(stone, man)) {

        //if there's a collision, change the message text
        //and tint the box red
        man.x = 0;
        // man.y = 0;

        message.text = "hit!";
        rectangle.tint = 0xccff99;

    } else {

        //if there's no collision, reset the message
        //text and the box's color
        message.text = "No collision...";
        rectangle.beginFill(0xccff99);
        rectangle.endFill();
        // rectangle.tint = 0xccff99;
    }


}

function hitTestRectangle(r1, r2) {

    //Define the variables we'll need to calculate
    var hit, combinedHalfWidths, combinedHalfHeights, vx, vy;

    //hit will determine whether there's a collision
    hit = false;

    //Find the center points of each sprite
    r1.centerX = r1.x + r1.width / 2;
    r1.centerY = r1.y + r1.height / 2;
    r2.centerX = r2.x + r2.width / 2;
    r2.centerY = r2.y + r2.height / 2;

    //Find the half-widths and half-heights of each sprite
    r1.halfWidth = r1.width / 2;
    r1.halfHeight = r1.height / 2;
    r2.halfWidth = r2.width / 2;
    r2.halfHeight = r2.height / 2;

    //Calculate the distance vector between the sprites
    vx = r1.centerX - r2.centerX;
    vy = r1.centerY - r2.centerY;

    //Figure out the combined half-widths and half-heights
    combinedHalfWidths = r1.halfWidth + r2.halfWidth;
    combinedHalfHeights = r1.halfHeight + r2.halfHeight;

    //Check for a collision on the x axis
    if (Math.abs(vx) < combinedHalfWidths) {

        //A collision might be occuring. Check for a collision on the y axis
        if (Math.abs(vy) < combinedHalfHeights) {

            //There's definitely a collision happening
            hit = true;
        } else {

            //There's no collision on the y axis
            hit = false;
        }
    } else {

        //There's no collision on the x axis
        hit = false;
    }

    //`hit` will be either `true` or `false`
    return hit;
};

function keyboard(keyCode) {
    var key = {};
    key.code = keyCode;
    key.isDown = false;
    key.isUp = true;
    key.press = undefined;
    key.release = undefined;
    //The `downHandler`
    key.downHandler = function (event) {
        if (event.keyCode === key.code) {
            if (key.isUp && key.press) key.press();
            key.isDown = true;
            key.isUp = false;
        }
        event.preventDefault();
    };

    //The `upHandler`
    key.upHandler = function (event) {
        if (event.keyCode === key.code) {
            if (key.isDown && key.release) key.release();
            key.isDown = false;
            key.isUp = true;
        }
        event.preventDefault();
    };

    //Attach event listeners
    window.addEventListener(
        "keydown", key.downHandler.bind(key), false
    );
    window.addEventListener(
        "keyup", key.upHandler.bind(key), false
    );
    return key;
}

function moveRight(vx) {
    ++start;
    man.vx = vx;
    man.vy = 0;
    var sprites = initThreeSprites();
    var sprites2 = initSprites();

    if (start <= 3) {

        baseImage.frame = sprites[start];
    }
    if (start > 3 && start <= 7) {
        baseImage.frame = sprites2[start];
    } else {
        start = 0;
    }
}


