/**
 * Created by andim on 11/28/2017.
 */

// create input interface
const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
